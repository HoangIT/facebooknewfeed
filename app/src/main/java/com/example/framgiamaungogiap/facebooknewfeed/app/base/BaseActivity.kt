package com.example.framgiamaungogiap.facebooknewfeed.app.base

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.os.Message
import android.support.v7.app.AppCompatActivity
import com.example.framgiamaungogiap.facebooknewfeed.extension.popBack
import com.example.framgiamaungogiap.facebooknewfeed.extension.toast

open abstract class BaseActivity : AppCompatActivity() {

    abstract fun getLayoutID(): Int

    abstract fun onViewReady()

    var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutID())
        onViewReady()
    }

    fun backToPrevious() {
        popBack()
    }

    fun makeToast(context: Context, message: String) {
        toast(context, message)
    }

    override fun onBackPressed() {
        popBack()
    }

    fun showLoaddingDialog(isCancel: Boolean) {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
            progressDialog = null
        }

        progressDialog = ProgressDialog(this)
        progressDialog!!.setCancelable(isCancel)
        progressDialog!!.setCanceledOnTouchOutside(isCancel)
        progressDialog!!.isIndeterminate = true
        progressDialog!!.setMessage("Loadding...")
        progressDialog!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog!!.show()

    }


    fun updateMessageLoaddingDialog(message: String) {
        if (progressDialog != null && progressDialog!!.isShowing()) {
            progressDialog!!.setMessage(message)
        }
    }


    fun dismisLoaddingDialog() {
        if (progressDialog != null && progressDialog!!.isShowing()) {
            progressDialog!!.dismiss()
        }
    }
}