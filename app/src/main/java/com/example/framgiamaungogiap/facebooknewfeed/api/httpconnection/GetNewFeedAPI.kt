package com.example.framgiamaungogiap.facebooknewfeed.api.httpconnection

import android.app.ProgressDialog
import android.content.Context
import android.os.AsyncTask
import com.example.framgiamaungogiap.facebooknewfeed.listener.OnRequestGetNewFeedListener
import com.example.framgiamaungogiap.facebooknewfeed.model.Feed
import com.example.framgiamaungogiap.facebooknewfeed.model.Stories
import com.example.framgiamaungogiap.facebooknewfeed.utils.AppConst
import com.google.gson.Gson
import org.json.JSONObject

class GetNewFeedAPI(context: Context, onRequestGetNewFeedListener: OnRequestGetNewFeedListener) : AsyncTask<String, Void, Void>() {

    var context: Context? = null
    var progressDialog: ProgressDialog? = null
    var onRequestGetNewFeedListener: OnRequestGetNewFeedListener? = null

    var arrStories = ArrayList<Stories>()
    var arrNewFeeds = ArrayList<Feed>()

    init {
        this.context = context
        this.onRequestGetNewFeedListener = onRequestGetNewFeedListener
        progressDialog = ProgressDialog(context)
    }


    override fun onPreExecute() {
        super.onPreExecute()
        progressDialog!!.setMessage("Loadding...")
        progressDialog!!.setCancelable(false)
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.show()

    }

    override fun doInBackground(vararg params: String?): Void? {
        arrNewFeeds.clear()
        arrStories.clear()

        var httpHandler = HTTPHandler()
        var jsonString = httpHandler.makeServiceCall(params[0]!!)

        if (jsonString != null) {


            var ob = JSONObject(jsonString)

            var arrayStories = ob.getJSONArray("stories")
            for (i in 0 until arrayStories.length()) {
                var obStories: JSONObject = arrayStories.getJSONObject(i)
                var fullname = obStories.getString("fullName")
                var storyImageUrl = obStories.getString("storyImageUrl")
                arrStories.add(Stories(fullname, storyImageUrl))
            }


            var arrayFeed = ob.getJSONArray("feeds")
            for (i in 0 until arrayFeed.length()) {
                var obFeed: JSONObject = arrayFeed.getJSONObject(i)

                var jsonString = obFeed.toString()
                var feed = Gson().fromJson<Feed>(jsonString, Feed::class.java)
                when (feed.feedImages.size) {
                    0 -> feed.viewType = AppConst.TYPE_FEED_ZERO
                    1 -> feed.viewType = AppConst.TYPE_FEED_ONE
                    2 -> feed.viewType = AppConst.TYPE_FEED_TWO
                    3 -> feed.viewType = AppConst.TYPE_FEED_THREE
                    in 4..1000 -> feed.viewType = AppConst.TYPE_FEED_MORE
                }
                arrNewFeeds.add(feed)
            }


        }

        return null
    }


    override fun onPostExecute(result: Void?) {
        super.onPostExecute(result)
        onRequestGetNewFeedListener!!.onSuccess(arrStories, arrNewFeeds)
        progressDialog!!.dismiss()
    }
}