package com.example.framgiamaungogiap.facebooknewfeed.app.fragment


import android.content.Intent
import android.view.View
import com.example.framgiamaungogiap.facebooknewfeed.R
import com.example.framgiamaungogiap.facebooknewfeed.app.activity.ProfileActivity
import com.example.framgiamaungogiap.facebooknewfeed.app.activity.RegisterLoginActivity
import com.example.framgiamaungogiap.facebooknewfeed.app.base.BaseFragment
import com.example.framgiamaungogiap.facebooknewfeed.utils.PreferUtils
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_setting.*
import java.io.File


class SettingFragment : BaseFragment(), View.OnClickListener {


    companion object {
        var fragment: SettingFragment? = null

        fun newInstance(): SettingFragment {
            var fragment = SettingFragment()

            return fragment!!
        }

        fun getInstance(): SettingFragment {
            return fragment!!
        }
    }

    override fun getLayoutID(): Int {
        return R.layout.fragment_setting
    }

    override fun onViewReady(view: View) {
        var user = PreferUtils.getUser(context!!)

        ll_view_profile.setOnClickListener(this)
        ll_log_out.setOnClickListener(this)

        tv_full_name.setText(user.fullname)

        var fileImage = File(user.imagePath)
        Picasso.with(context).load(fileImage).into(imv_avatar)
    }


    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ll_log_out -> {
                var intent = Intent(activity!!, RegisterLoginActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
            }

            R.id.ll_view_profile -> {
                var intent: Intent = Intent(activity, ProfileActivity::class.java)
                startActivity(intent)
            }
        }

    }
}
