package com.example.framgiamaungogiap.facebooknewfeed.model

import com.example.framgiamaungogiap.facebooknewfeed.utils.APIConfig
import com.example.framgiamaungogiap.facebooknewfeed.utils.AppConst
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Feed {

    @SerializedName(APIConfig.KEY_RESPONSE_FULLNAME)
    @Expose
    var fullName: String = AppConst.BLANK

    @SerializedName(APIConfig.KEY_RESPONSE_AVATARURL)
    @Expose
    var avatarUrl: String = AppConst.BLANK

    @SerializedName(APIConfig.KEY_RESPONSE_CREATEAT)
    @Expose
    var createAt: String = AppConst.BLANK

    @SerializedName(APIConfig.KEY_RESPONSE_FEED_CONTENT)
    @Expose
    var feedContent: String = AppConst.BLANK

    @SerializedName(APIConfig.KEY_RESPONSE_VISIBLE_MODE)
    @Expose
    var visibleMode: String = AppConst.BLANK

    @SerializedName(APIConfig.KEY_RESPONSE_REACTION_COUNT)
    @Expose
    var reactionCount: Int = 0

    @SerializedName(APIConfig.KEY_RESPONSE_COMMENT_COUNT)
    @Expose
    var commentCount: Int = 0

    @SerializedName(APIConfig.KEY_RESPONSE_SHARING_COUNT)
    @Expose
    var sharingCount: Int = 0

    @SerializedName(APIConfig.KEY_RESPONSE_FEED_IMAGES)
    @Expose
    var feedImages = arrayListOf<String>()

    var viewType: Int = -1

    constructor(fullname: String, avatarUrl: String, createAt: String, feedContent: String, visibleMode: String, reactionCount: Int, commentCount: Int, sharingCount: Int, feedImages: ArrayList<String>) {
        this.fullName = fullname
        this.avatarUrl = avatarUrl
        this.createAt = createAt
        this.feedContent = feedContent
        this.visibleMode = visibleMode
        this.reactionCount = reactionCount
        this.commentCount = commentCount
        this.sharingCount = sharingCount
        this.feedImages = feedImages
    }
}