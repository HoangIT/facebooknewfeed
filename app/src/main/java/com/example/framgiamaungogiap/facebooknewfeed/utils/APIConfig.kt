package com.example.framgiamaungogiap.facebooknewfeed.utils

class APIConfig {
    companion object {
        const val BASE_URL = "http://www.mocky.io/v2/"
        const val URL_NEW_FEED = "5b0ce0cb3300005100b400f1"

        const val KEY_RESPONSE_STATUS = "status_code"
        const val KEY_RESPONSE_MESSAGE = "message"
        const val KEY_RESPONSE_DATA = "data"

        const val KEY_RESPONSE_FULLNAME = "fullName"
        const val KEY_RESPONSE_STORY_IMAGE_URL = "storyImageUrl"
        const val KEY_RESPONSE_LIST_STORIES = "stories"
        const val KEY_RESPONSE_LIST_FEEDS = "feeds"
        const val KEY_RESPONSE_AVATARURL = "avatarUrl"
        const val KEY_RESPONSE_CREATEAT = "createAt"
        const val KEY_RESPONSE_FEED_CONTENT = "feedContent"
        const val KEY_RESPONSE_VISIBLE_MODE = "visibleMode"
        const val KEY_RESPONSE_REACTION_COUNT = "reactionCount"
        const val KEY_RESPONSE_COMMENT_COUNT = "commentCount"
        const val KEY_RESPONSE_SHARING_COUNT = "sharingCount"
        const val KEY_RESPONSE_FEED_IMAGES = "feedImages"
    }
}