package com.example.framgiamaungogiap.facebooknewfeed.model

import com.example.framgiamaungogiap.facebooknewfeed.utils.APIConfig
import com.example.framgiamaungogiap.facebooknewfeed.utils.AppConst
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Stories {
    @SerializedName(APIConfig.KEY_RESPONSE_FULLNAME)
    @Expose
    var fullName: String = AppConst.BLANK

    @SerializedName(APIConfig.KEY_RESPONSE_STORY_IMAGE_URL)
    @Expose
    var storyImageUrl: String = AppConst.BLANK

    constructor(fullName: String, storyImageUrl: String) {
        this.fullName = fullName
        this.storyImageUrl = storyImageUrl
    }
}