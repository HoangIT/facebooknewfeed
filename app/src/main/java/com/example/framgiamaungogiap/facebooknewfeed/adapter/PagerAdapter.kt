package com.example.framgiamaungogiap.facebooknewfeed.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.example.framgiamaungogiap.facebooknewfeed.app.fragment.NewFeedFragment
import com.example.framgiamaungogiap.facebooknewfeed.app.fragment.SettingFragment
import com.example.framgiamaungogiap.facebooknewfeed.app.fragment.TempFragment

class PagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> {
                return NewFeedFragment.newInstance()
            }

            1 -> {
                return TempFragment()
            }

            2 -> {
                return TempFragment()
            }

            3 -> {
                return SettingFragment.newInstance()
            }
        }
        return TempFragment()
    }

    override fun getCount(): Int {
        return 4
    }
}