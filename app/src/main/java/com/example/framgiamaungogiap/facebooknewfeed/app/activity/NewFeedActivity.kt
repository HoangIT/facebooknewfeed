package com.example.framgiamaungogiap.facebooknewfeed.app.activity

import com.example.framgiamaungogiap.facebooknewfeed.adapter.PagerAdapter
import com.example.framgiamaungogiap.facebooknewfeed.R
import com.example.framgiamaungogiap.facebooknewfeed.app.base.BaseActivity
import kotlinx.android.synthetic.main.activity_new_feed.*

class NewFeedActivity : BaseActivity() {


    override fun getLayoutID(): Int {
        return R.layout.activity_new_feed
    }

    override fun onViewReady() {
        var pagerAdapter: PagerAdapter = PagerAdapter(supportFragmentManager)
        view_pager.adapter = pagerAdapter
//        tabs_layout.setupWithViewPager(view_pager)

    }
}
