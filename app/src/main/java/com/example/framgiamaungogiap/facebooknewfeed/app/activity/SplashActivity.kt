package com.example.framgiamaungogiap.facebooknewfeed.app.activity

import android.content.Intent
import android.os.Handler
import com.example.framgiamaungogiap.facebooknewfeed.R
import com.example.framgiamaungogiap.facebooknewfeed.app.base.BaseActivity
import com.example.framgiamaungogiap.facebooknewfeed.utils.PreferUtils

class SplashActivity : BaseActivity() {

    override fun getLayoutID(): Int {
        return R.layout.activity_splash
    }

    override fun onViewReady() {
        Handler().postDelayed({
            var user = PreferUtils.getUser(this)
            if (user.isEmpty()) {
                var intent: Intent = Intent(this, RegisterLoginActivity::class.java)
                startActivity(intent)
            } else {
                var intent: Intent = Intent(this, NewFeedActivity::class.java)
                startActivity(intent)
            }
            finish()
        }, 2000)
    }

}
