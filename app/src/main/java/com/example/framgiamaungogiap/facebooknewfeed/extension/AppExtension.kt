package com.example.framgiamaungogiap.facebooknewfeed.extension

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.example.framgiamaungogiap.facebooknewfeed.R

fun AppCompatActivity.replaceFragment(rootID: Int, fragment: Fragment, isAddTobackStack: Boolean) {
    if (isAddTobackStack) {
        supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit, R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
                .replace(rootID, fragment, fragment::class.java.simpleName)
                .addToBackStack(fragment::class.java.simpleName).commit()
    } else {
        supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit, R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
                .replace(rootID, fragment, fragment::class.java.simpleName)
                .commit()
    }
}

fun AppCompatActivity.addFragment(rootID: Int, fragment: Fragment, isAddTobackStack: Boolean) {
    if (isAddTobackStack) {
        supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit, R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
                .add(rootID, fragment, fragment::class.java.simpleName)
                .addToBackStack(fragment::class.java.simpleName).commit()
    } else {
        supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit, R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit)
                .add(rootID, fragment, fragment::class.java.simpleName)
                .commit()
    }
}

fun AppCompatActivity.popBack() {
    if (supportFragmentManager.backStackEntryCount > 0) {
        if (supportFragmentManager.popBackStackImmediate()) {
            Log.d("", "popBackStackImmediate, success")
        } else {
            Log.d("", "popBackStackImmediate, fail")
        }
    } else {
        finish()
    }
}

fun AppCompatActivity.toast(context: Context, message: String) {
    Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
}