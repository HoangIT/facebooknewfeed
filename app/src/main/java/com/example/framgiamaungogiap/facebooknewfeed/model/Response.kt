package com.example.framgiamaungogiap.facebooknewfeed.model

import com.example.framgiamaungogiap.facebooknewfeed.utils.APIConfig
import com.example.framgiamaungogiap.facebooknewfeed.utils.AppConst
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Response {

    @SerializedName(APIConfig.KEY_RESPONSE_STATUS)
    @Expose
    var statusCode: Int? = 0

    @SerializedName(APIConfig.KEY_RESPONSE_MESSAGE)
    @Expose
    var message: String? = AppConst.BLANK

    @SerializedName(APIConfig.KEY_RESPONSE_DATA)
    @Expose
    var responseData: ResponseData? = null
}