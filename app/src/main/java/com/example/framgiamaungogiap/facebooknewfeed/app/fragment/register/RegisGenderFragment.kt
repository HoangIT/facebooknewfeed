package com.example.framgiamaungogiap.facebooknewfeed.app.fragment.register


import android.annotation.SuppressLint
import android.view.View
import com.example.framgiamaungogiap.facebooknewfeed.R
import com.example.framgiamaungogiap.facebooknewfeed.app.base.BaseFragment
import com.example.framgiamaungogiap.facebooknewfeed.model.User
import kotlinx.android.synthetic.main.fragment_regis_gender.*
import kotlinx.android.synthetic.main.tool_bar_frag.*


@SuppressLint("ValidFragment")
class RegisGenderFragment(user: User) : BaseFragment(), View.OnClickListener {

    var user: User? = null

    init {
        this.user = user
    }

    override fun getLayoutID(): Int {
        return R.layout.fragment_regis_gender
    }

    override fun onViewReady(view: View) {
        tv_title.setText("Gender")
        imv_back.setOnClickListener(this)
        btn_next.setOnClickListener(this)
    }


    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.imv_back -> {
                backToPrevious()
            }

            R.id.btn_next -> {
                var gender: String = ""
                if (rd_male.isChecked) {
                    gender = "Male"
                } else if (rd_female.isChecked) {
                    gender = "Female"
                }
                user!!.gender = gender
                replaceFragment(R.id.fragment_container, RegisPhoneFragment(user!!), true)
            }
        }

    }

}
