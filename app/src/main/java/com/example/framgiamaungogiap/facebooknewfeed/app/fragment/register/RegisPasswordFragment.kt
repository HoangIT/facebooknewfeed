package com.example.framgiamaungogiap.facebooknewfeed.app.fragment.register


import android.annotation.SuppressLint
import android.view.View
import com.example.framgiamaungogiap.facebooknewfeed.R
import com.example.framgiamaungogiap.facebooknewfeed.app.base.BaseFragment
import com.example.framgiamaungogiap.facebooknewfeed.model.User
import kotlinx.android.synthetic.main.fragment_regis_password.*
import kotlinx.android.synthetic.main.tool_bar_frag.*


@SuppressLint("ValidFragment")
class RegisPasswordFragment(user: User) : BaseFragment(), View.OnClickListener {

    var user: User? = null

    init {
        this.user = user
    }

    override fun getLayoutID(): Int {
        return R.layout.fragment_regis_password
    }

    override fun onViewReady(view: View) {
        tv_title.setText("Password")
        imv_back.setOnClickListener(this)
        btn_next.setOnClickListener(this)
    }


    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.imv_back -> {
                backToPrevious()
            }

            R.id.btn_next -> {
                var pass = edt_password.text.toString()
                if (pass == "") {
                    makeToast(context!!, "Input value")
                } else {
                    user!!.password = edt_password.text.toString()
                    replaceFragment(R.id.fragment_container, FinishRegisFragment(user!!), true)
                }
            }
        }

    }

}
