package com.example.framgiamaungogiap.facebooknewfeed.app.fragment.register


import android.view.View
import com.example.framgiamaungogiap.facebooknewfeed.R
import com.example.framgiamaungogiap.facebooknewfeed.app.base.BaseFragment
import com.example.framgiamaungogiap.facebooknewfeed.model.User
import kotlinx.android.synthetic.main.fragment_regis_name.*
import kotlinx.android.synthetic.main.tool_bar_frag.*


class RegisNameFragment : BaseFragment(), View.OnClickListener {


    override fun getLayoutID(): Int {
        return R.layout.fragment_regis_name
    }

    override fun onViewReady(view: View) {
        tv_title.setText("Name")
        imv_back.setOnClickListener(this)
        btn_next.setOnClickListener(this)
    }


    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.imv_back -> {
                backToPrevious()
            }

            R.id.btn_next -> {
                var firstName = edt_first_name.text.toString()
                var lastName = edt_last_name.text.toString()

                if (firstName == "" && lastName == "") {
                    makeToast(context!!, "Input value")
                } else {
                    var user: User = User()
                    user.fullname = firstName + " " + lastName
                    replaceFragment(R.id.fragment_container, RegisBirthdayFragment(user), true)
                }
            }
        }

    }

}
