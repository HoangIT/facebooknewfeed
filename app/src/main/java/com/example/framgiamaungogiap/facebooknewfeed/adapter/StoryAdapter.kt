package com.example.framgiamaungogiap.facebooknewfeed.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.framgiamaungogiap.facebooknewfeed.R
import com.example.framgiamaungogiap.facebooknewfeed.model.Stories
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView

class StoryAdapter(context: Context, arrStories: ArrayList<Stories>?) : RecyclerView.Adapter<StoryAdapter.ViewHolder>() {

    var context: Context? = null
    private var arrStories: ArrayList<Stories>? = ArrayList()


    init {
        this.context = context
        this.arrStories = arrStories
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.item_stories, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return arrStories!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var urlImage = arrStories!![position].storyImageUrl
        Picasso.with(context).load(urlImage).placeholder(R.drawable.ic_avatar_sample).error(R.drawable.ic_avatar_sample).into(holder.imvImage)

        holder.tvFullname.text = arrStories!![position].fullName
    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var imvImage = view.findViewById<CircleImageView>(R.id.imv_stories_item)!!
        var tvFullname = view.findViewById<TextView>(R.id.tv_full_name_stories)
    }
}