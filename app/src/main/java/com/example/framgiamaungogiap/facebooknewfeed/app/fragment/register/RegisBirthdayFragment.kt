package com.example.framgiamaungogiap.facebooknewfeed.app.fragment.register


import android.annotation.SuppressLint
import android.view.View
import com.example.framgiamaungogiap.facebooknewfeed.R
import com.example.framgiamaungogiap.facebooknewfeed.app.base.BaseFragment
import com.example.framgiamaungogiap.facebooknewfeed.model.User
import kotlinx.android.synthetic.main.fragment_regis_birthday.*
import kotlinx.android.synthetic.main.tool_bar_frag.*


@SuppressLint("ValidFragment")
class RegisBirthdayFragment(user: User) : BaseFragment(), View.OnClickListener {

    var user: User? = null

    init {
        this.user = user
    }

    override fun getLayoutID(): Int {
        return R.layout.fragment_regis_birthday
    }

    override fun onViewReady(view: View) {
        tv_title.setText("Birthday")
        imv_back.setOnClickListener(this)
        btn_next.setOnClickListener(this)
    }


    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.imv_back -> {
                backToPrevious()
            }

            R.id.btn_next -> {
                var day = edt_day.text.toString()
                var month = edt_month.text.toString()
                var year = edt_year.text.toString()

                if (day == "" && month == "" && year == "") {
                    makeToast(context!!, "Input value")
                } else {
                    var bithDay = day + "/" + month + "/" + year
                    user!!.dateBirthday = bithDay
                    replaceFragment(R.id.fragment_container, RegisGenderFragment(user!!), true)
                }
            }
        }

    }

}
