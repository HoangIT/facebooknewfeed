package com.example.framgiamaungogiap.facebooknewfeed.listener

import com.example.framgiamaungogiap.facebooknewfeed.model.Feed
import com.example.framgiamaungogiap.facebooknewfeed.model.Stories

interface OnRequestGetNewFeedListener {
    fun onSuccess(arrStories: ArrayList<Stories>, arrFeed: ArrayList<Feed>)
}