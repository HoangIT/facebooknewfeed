package com.example.framgiamaungogiap.facebooknewfeed.utils

class AppConst {

    companion object {
        const val BLANK = ""

        const val FILE_SETTING_NAME = "setting_user"
        const val KEY_USER_INFOR = "key_user_infor"
        const val GALLERY_REQUEST_CODE = 12
        const val CAMERA_REQUEST_CODE = 13
        const val PERMISSION_REQUEST_CODE = 14

        const val URI_EXTERNAL_STORAGE_DOCUMENT = "com.android.externalstorage.documents"
        const val URI_DOWNLOAD_DOCUMENT = "com.android.providers.downloads.documents"
        const val URI_MEDIA_DOCUMENT = "com.android.providers.media.documents"
        const val URI_GOOGLE_PHOTO_DOCUMENT = "com.google.android.apps.photos.content"


        const val TYPE_FEED_ZERO: Int = 110
        const val TYPE_FEED_ONE: Int = 111
        const val TYPE_FEED_TWO: Int = 112
        const val TYPE_FEED_THREE: Int = 113
        const val TYPE_FEED_MORE: Int = 114
        const val TYPE_FEED_HEADER: Int = 115

    }
}