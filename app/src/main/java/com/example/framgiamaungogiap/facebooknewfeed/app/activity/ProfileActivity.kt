package com.example.framgiamaungogiap.facebooknewfeed.app.activity

import com.example.framgiamaungogiap.facebooknewfeed.R
import com.example.framgiamaungogiap.facebooknewfeed.app.base.BaseActivity
import com.example.framgiamaungogiap.facebooknewfeed.utils.PreferUtils
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_profile.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import java.io.File

class ProfileActivity : BaseActivity() {


    override fun getLayoutID(): Int {
        return R.layout.activity_profile
    }

    override fun onViewReady() {
        var user = PreferUtils.getUser(this)
        var fileImage = File(user.imagePath)

        tv_full_name.setText(user.fullname)
        Picasso.with(this).load(fileImage).into(imv_avatar)
        Picasso.with(this).load(R.drawable.ic_cover).into(imv_cover)

        imv_back.onClick {
            finish()
        }
    }

}
