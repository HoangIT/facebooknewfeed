package com.example.framgiamaungogiap.facebooknewfeed.app.fragment.register


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.view.View
import com.example.framgiamaungogiap.facebooknewfeed.R
import com.example.framgiamaungogiap.facebooknewfeed.app.base.BaseFragment
import com.example.framgiamaungogiap.facebooknewfeed.model.User
import com.example.framgiamaungogiap.facebooknewfeed.utils.AppConst
import com.example.framgiamaungogiap.facebooknewfeed.utils.Utilities
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_regis_avatar.*
import kotlinx.android.synthetic.main.tool_bar_frag.*
import org.jetbrains.anko.support.v4.startActivity
import java.io.File
import java.io.IOError
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import android.graphics.Matrix
import android.media.ExifInterface
import android.opengl.ETC1.getHeight
import android.opengl.ETC1.getWidth
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.util.DisplayMetrics
import com.squareup.picasso.Callback


@SuppressLint("ValidFragment")
class RegisAvatarFragment(user: User) : BaseFragment(), View.OnClickListener {

    var user: User? = null

    private var mCurrentPhotoPath: String? = ""


    init {
        this.user = user
    }

    override fun getLayoutID(): Int {
        return R.layout.fragment_regis_avatar
    }

    override fun onViewReady(view: View) {
        tv_title.setText("Avatar")
        imv_back.setOnClickListener(this)
        btn_next.setOnClickListener(this)
        btn_take_from_camera.setOnClickListener(this)
        btn_take_from_gallery.setOnClickListener(this)
    }


    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.imv_back -> {
                backToPrevious()
            }

            R.id.btn_next -> {
                if (mCurrentPhotoPath != "") {
                    replaceFragment(R.id.fragment_container, RegisPasswordFragment(user!!), true)
                } else {
                    makeToast(context!!, "Please choose image")
                }
            }

            R.id.btn_take_from_camera -> {
                requestFromCamera()
            }

            R.id.btn_take_from_gallery -> {
                if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(context!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        var permission = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        ActivityCompat.requestPermissions(activity!!, permission, AppConst.PERMISSION_REQUEST_CODE)
                    }
                } else {
                    requestFromGallery()
                }
            }
        }

    }

    fun requestFromGallery() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "image/*"
        if (intent.resolveActivity(activity!!.packageManager) != null) {
            startActivityForResult(intent, AppConst.GALLERY_REQUEST_CODE)
        }
    }

    fun requestFromCamera() {
        var takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(activity!!.packageManager) != null) {
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                ex.printStackTrace()
            }

            if (photoFile != null) {
                var photoURI: Uri = FileProvider.getUriForFile(context!!, "com.example.framgiamaungogiap.facebooknewfeed.fileprovider", photoFile)
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, AppConst.CAMERA_REQUEST_CODE)
            }

        }
    }

    private fun createImageFile(): File {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = activity!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
                imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir      /* directory */
        )

        mCurrentPhotoPath = image.getAbsolutePath()
        return image
    }

    private fun galleryAddPic() {
        val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        val f = File(mCurrentPhotoPath)
        val contentUri = Uri.fromFile(f)
        mediaScanIntent.data = contentUri
        activity!!.sendBroadcast(mediaScanIntent)
    }


    private fun setPic() {
        // Get the dimensions of the Screen
        val displayMetrics = DisplayMetrics()
        activity!!.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics)

        val targetW = displayMetrics.widthPixels
        val targetH = displayMetrics.heightPixels

        // Get the dimensions of the bitmap
        val bounds = BitmapFactory.Options()
        bounds.inJustDecodeBounds = true
        BitmapFactory.decodeFile(mCurrentPhotoPath, bounds)
        val photoW = bounds.outWidth
        val photoH = bounds.outHeight

        // Determine how much to scale down the image
        val scaleFactor = Math.min(photoW / targetW, photoH / targetH)

        // Decode the image file into a Bitmap sized to fill the View
        val bmOptions = BitmapFactory.Options()
        bmOptions.inJustDecodeBounds = false
        bmOptions.inSampleSize = scaleFactor
        bmOptions.inPurgeable = true
        bmOptions.inDither = true
        bmOptions.inPreferredConfig = Bitmap.Config.RGB_565

        var bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions)
        var rotaBitmap: Bitmap? = null
        try {
            val exif = ExifInterface(mCurrentPhotoPath)
            val orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION)
            val orientation = if (orientString != null) Integer.parseInt(orientString) else ExifInterface.ORIENTATION_NORMAL

            var rotationAngle = 0
            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90
            if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180
            if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270

            val matrix = Matrix()
            matrix.setRotate(rotationAngle.toFloat(), bitmap.width.toFloat() / 2, bitmap.height.toFloat() / 2)
            rotaBitmap = Bitmap.createBitmap(bitmap, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        imv_avatar.setImageBitmap(rotaBitmap)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            AppConst.GALLERY_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    val imgPickedUri = data!!.data
                    mCurrentPhotoPath = Utilities.getGalleryPathFromURI(context!!, imgPickedUri)
                    var file = File(mCurrentPhotoPath)
                    Picasso.with(context).load(file).into(imv_avatar)

                    user!!.imagePath = mCurrentPhotoPath!!
                }
            }

            AppConst.CAMERA_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    setPic()
                    galleryAddPic()
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            AppConst.PERMISSION_REQUEST_CODE -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    requestFromGallery()
                } else {
                    makeToast(context!!, "Permision denied")
                }
            }
        }
    }

}
