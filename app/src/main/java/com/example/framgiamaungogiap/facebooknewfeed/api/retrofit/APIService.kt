package com.example.framgiamaungogiap.facebooknewfeed.api.retrofit

import com.example.framgiamaungogiap.facebooknewfeed.model.ResponseData
import com.example.framgiamaungogiap.facebooknewfeed.utils.APIConfig
import retrofit2.Call
import retrofit2.http.GET

interface APIService {

    @GET(APIConfig.URL_NEW_FEED)
    fun getDataNewFeed(): Call<ResponseData>
}