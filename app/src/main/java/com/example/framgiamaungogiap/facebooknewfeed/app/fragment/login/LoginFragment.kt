package com.example.framgiamaungogiap.facebooknewfeed.app.fragment.login


import android.content.Intent
import android.view.View
import android.widget.Toast
import com.example.framgiamaungogiap.facebooknewfeed.R
import com.example.framgiamaungogiap.facebooknewfeed.app.activity.NewFeedActivity
import com.example.framgiamaungogiap.facebooknewfeed.app.base.BaseFragment
import com.example.framgiamaungogiap.facebooknewfeed.app.fragment.register.RegisNameFragment
import com.example.framgiamaungogiap.facebooknewfeed.utils.PreferUtils
import kotlinx.android.synthetic.main.fragment_login.*


class LoginFragment : BaseFragment(), View.OnClickListener {

    override fun getLayoutID(): Int {
        return R.layout.fragment_login
    }

    override fun onViewReady(view: View) {
        setEvent()

    }

    private fun setEvent() {
        btn_login.setOnClickListener(this)
        btn_regis_account.setOnClickListener(this)
    }


    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btn_login -> {
                var user = PreferUtils.getUser(context!!)
                var id = user.phone
                var pass = user.password

                if (edt_password.text.toString() != "" && edt_username.text.toString() != "") {
                    if (id == edt_username.text.toString()) {
                        if (pass == edt_password.text.toString()) {

                            var intent: Intent = Intent(activity, NewFeedActivity::class.java)
                            startActivity(intent)

                        } else {
                            makeToast(context!!, "Password invalid")
                        }
                    } else {
                        makeToast(context!!, "ID invalid")
                    }
                } else {
                    makeToast(context!!, "input value")
                }
            }

            R.id.btn_regis_account -> {
                replaceFragment(R.id.fragment_container, RegisNameFragment(), true)
            }
        }
    }

}
