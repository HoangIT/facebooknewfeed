package com.example.framgiamaungogiap.facebooknewfeed.api.retrofit

class APIUtils {
    companion object {
        fun getAPIService(): APIService {
            return RetrofitClient.getClient()!!.create(APIService::class.java)
        }
    }
}