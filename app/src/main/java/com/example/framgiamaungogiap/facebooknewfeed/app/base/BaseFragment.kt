package com.example.framgiamaungogiap.facebooknewfeed.app.base

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.framgiamaungogiap.facebooknewfeed.extension.replaceFragment

open abstract class BaseFragment : Fragment() {

    abstract fun getLayoutID(): Int

    abstract fun onViewReady(view: View)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onViewReady(view)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(getLayoutID(), container, false)
    }


    fun replaceFragment(rootId: Int, fragment: Fragment, isAddToBackStack: Boolean) {
        if (activity is BaseActivity) {
            (activity as BaseActivity).replaceFragment(rootId, fragment, isAddToBackStack)
        }
    }

    fun makeToast(context: Context, message: String) {
        if (activity is BaseActivity) {
            (activity as BaseActivity).makeToast(context, message)
        }
    }


    fun backToPrevious() {
        if (activity is BaseActivity) {
            (activity as BaseActivity).backToPrevious()
        }
    }

    fun finishActivity() {
        if (activity is BaseActivity) {
            (activity as BaseActivity).finish()
        }
    }

    fun showLoaddingDialog(isCancel: Boolean) {
        if (activity is BaseActivity) {
            (activity as BaseActivity).showLoaddingDialog(isCancel)
        }
    }

    fun updateMessageLoaddingDialog(message: String) {
        if (activity is BaseActivity) {
            (activity as BaseActivity).updateMessageLoaddingDialog(message)
        }
    }

    fun dismisLoaddingDialog() {
        if (activity is BaseActivity) {
            (activity as BaseActivity).dismisLoaddingDialog()
        }
    }

}