package com.example.framgiamaungogiap.facebooknewfeed.model

class User {

    var fullname: String = ""
    var gender: String = ""
    var phone: String = ""
    var dateBirthday: String = ""
    var imagePath: String = ""
    var password: String = ""

    constructor()

    constructor(fullname: String, gender: String, phone: String, dateBirthday: String) {
        this.fullname = fullname
        this.gender = gender
        this.phone = phone
        this.dateBirthday = dateBirthday
    }

    fun isEmpty(): Boolean {
        if (fullname == "") {
            return true
        }

        if (gender == "") {
            return true
        }

        if (phone == "") {
            return true
        }

        if (dateBirthday == "") {
            return true
        }

        if (password == "") {
            return true
        }
        return false
    }
}