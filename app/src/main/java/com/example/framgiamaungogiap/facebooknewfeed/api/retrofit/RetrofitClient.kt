package com.example.framgiamaungogiap.facebooknewfeed.api.retrofit

import com.example.framgiamaungogiap.facebooknewfeed.utils.APIConfig
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitClient {

    companion object {
        var retrofit: Retrofit? = null

        var okHttp = OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build()

        fun getClient(): Retrofit? {
            if (retrofit == null) {
                retrofit = Retrofit.Builder().baseUrl(APIConfig.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(okHttp)
                        .build()
            }
            return retrofit
        }
    }
}