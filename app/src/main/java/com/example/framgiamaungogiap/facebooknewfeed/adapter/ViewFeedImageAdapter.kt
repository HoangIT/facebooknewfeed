package com.example.framgiamaungogiap.facebooknewfeed.adapter

import android.app.Activity
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import com.example.framgiamaungogiap.facebooknewfeed.R
import com.squareup.picasso.Picasso


class ViewFeedImageAdapter(activity: Activity, data: ArrayList<String>) : PagerAdapter() {

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object` as RelativeLayout
    }

    var activity: Activity? = null
    private var dataList: ArrayList<String>? = null
    private var inflater: LayoutInflater? = null

    init {
        this.activity = activity
        this.dataList = data
        this.inflater = LayoutInflater.from(activity)
    }


    override fun getCount(): Int {
        if (dataList != null) {
            return dataList!!.size
        }
        return 0
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        var view = inflater!!.inflate(R.layout.feed_image_preview, container, false)
        var data = dataList!![position]
        var imageView = view.findViewById<ImageView>(R.id.item_image)
        if (data != "") {
            Picasso.with(activity).load(data).into(imageView)
        }
        (container as ViewPager).addView(view, 0)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        (container as ViewPager).removeView(`object` as RelativeLayout)
    }
}