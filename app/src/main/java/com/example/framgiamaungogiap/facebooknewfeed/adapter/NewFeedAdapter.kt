package com.example.framgiamaungogiap.facebooknewfeed.adapter

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.framgiamaungogiap.facebooknewfeed.R
import com.example.framgiamaungogiap.facebooknewfeed.listener.OnViewPhotoFeedListenr
import com.example.framgiamaungogiap.facebooknewfeed.model.Feed
import com.example.framgiamaungogiap.facebooknewfeed.model.Stories
import com.example.framgiamaungogiap.facebooknewfeed.utils.AppConst
import com.example.framgiamaungogiap.facebooknewfeed.utils.Utilities
import com.squareup.picasso.Picasso
import org.jetbrains.anko.imageResource
import java.util.*

class NewFeedAdapter(context: Context, arrNewFeeds: ArrayList<Feed>, arrStories: ArrayList<Stories>, onViewPhotoFeedListenr: OnViewPhotoFeedListenr?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    var arrNewFeeds: ArrayList<Feed>? = null
    var arrStories: ArrayList<Stories>? = null
    var context: Context? = null
    var onViewPhotoFeedListenr: OnViewPhotoFeedListenr? = null

    init {
        this.context = context
        this.arrNewFeeds = arrNewFeeds
        this.arrStories = arrStories
        this.onViewPhotoFeedListenr = onViewPhotoFeedListenr
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var view: View? = null
        when (viewType) {
            AppConst.TYPE_FEED_ZERO -> view = LayoutInflater.from(context!!).inflate(R.layout.item_new_feed_0, parent, false)
            AppConst.TYPE_FEED_ONE -> view = LayoutInflater.from(context!!).inflate(R.layout.item_new_feed_1, parent, false)
            AppConst.TYPE_FEED_TWO -> view = LayoutInflater.from(context!!).inflate(R.layout.item_new_feed_2, parent, false)
            AppConst.TYPE_FEED_THREE -> view = LayoutInflater.from(context!!).inflate(R.layout.item_new_feed_3, parent, false)
            AppConst.TYPE_FEED_MORE -> view = LayoutInflater.from(context!!).inflate(R.layout.item_new_feed_more, parent, false)
            AppConst.TYPE_FEED_HEADER -> {
                view = LayoutInflater.from(context!!).inflate(R.layout.header_new_feed, parent, false)
                return HeaderHolder(view, viewType)
            }
        }


        return ViewHolder(view!!, viewType)
    }

    override fun getItemCount(): Int {
        return arrNewFeeds!!.size + 1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (position == 0) {
            holder as HeaderHolder
            holder.storyAdapter!!.notifyDataSetChanged()
        } else {
            var pos = position - 1
            holder as ViewHolder
            var feedItem = arrNewFeeds!![pos]
            //Header
            var avatarUrl = feedItem.avatarUrl
            Picasso.with(context).load(avatarUrl)
                    .placeholder(R.drawable.ic_avatar_sample)
                    .error(R.drawable.ic_avatar_sample)
                    .into(holder.imvAvatarUserFeed!!)

            holder.tvFullNameFeed!!.text = feedItem.fullName
            holder.tvPubdateFeed!!.text = Utilities.convertStringDateToString(feedItem.createAt)

            if (feedItem.visibleMode == "public") {
                holder.imvVisibleMode!!.imageResource = R.drawable.ic_public
            } else if (feedItem.visibleMode == "friends") {
                holder.imvVisibleMode!!.imageResource = R.drawable.ic_friends
            } else if (feedItem.visibleMode == "onlyme") {
                holder.imvVisibleMode!!.imageResource = R.drawable.ic_only_me
            }
            holder.tvContentFeed!!.text = feedItem.feedContent

            //Content
            var viewType = feedItem.viewType
            when (viewType) {
                AppConst.TYPE_FEED_ONE -> {
                    Picasso.with(context).load(feedItem.feedImages[0])
                            .placeholder(R.drawable.bg_image_loadding)
                            .error(R.drawable.bg_image_loadding)
                            .into(holder.imvPhotoView1)
                }

                AppConst.TYPE_FEED_TWO -> {
                    Picasso.with(context).load(feedItem.feedImages[0])
                            .placeholder(R.drawable.bg_image_loadding)
                            .error(R.drawable.bg_image_loadding)
                            .into(holder.imvPhotoView1)
                    Picasso.with(context).load(feedItem.feedImages[1])
                            .placeholder(R.drawable.bg_image_loadding)
                            .error(R.drawable.bg_image_loadding)
                            .into(holder.imvPhotoView2)
                }

                AppConst.TYPE_FEED_THREE -> {
                    Picasso.with(context).load(feedItem.feedImages[0])
                            .placeholder(R.drawable.bg_image_loadding)
                            .error(R.drawable.bg_image_loadding)
                            .into(holder.imvPhotoView1)
                    Picasso.with(context).load(feedItem.feedImages[1])
                            .placeholder(R.drawable.bg_image_loadding)
                            .error(R.drawable.bg_image_loadding)
                            .into(holder.imvPhotoView2)
                    Picasso.with(context).load(feedItem.feedImages[2])
                            .placeholder(R.drawable.bg_image_loadding)
                            .error(R.drawable.bg_image_loadding)
                            .into(holder.imvPhotoView3)
                }

                AppConst.TYPE_FEED_MORE -> {
                    Picasso.with(context).load(feedItem.feedImages[0])
                            .placeholder(R.drawable.bg_image_loadding)
                            .error(R.drawable.bg_image_loadding)
                            .into(holder.imvPhotoView1)
                    Picasso.with(context).load(feedItem.feedImages[1])
                            .placeholder(R.drawable.bg_image_loadding)
                            .error(R.drawable.bg_image_loadding)
                            .into(holder.imvPhotoView2)
                    Picasso.with(context).load(feedItem.feedImages[2])
                            .placeholder(R.drawable.bg_image_loadding)
                            .error(R.drawable.bg_image_loadding)
                            .into(holder.imvPhotoView3)
                    holder.tvMoreAlbum!!.text = "+" + (feedItem.feedImages.size - 3).toString()
                }
            }

            //Footer
            holder.tvReactionCount!!.text = feedItem.reactionCount.toString()
            holder.tvCommentCount!!.text = feedItem.commentCount.toString() + " comments"
            holder.tvSharingCount!!.text = feedItem.sharingCount.toString() + " sharings"
        }

    }

    override fun getItemViewType(position: Int): Int {
        if (position == 0) {
            return AppConst.TYPE_FEED_HEADER
        }
        var pos = position - 1
        return arrNewFeeds!![pos].viewType
    }

    inner class HeaderHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var rcStories: RecyclerView? = null
        var storyAdapter: StoryAdapter? = null

        constructor(itemView: View, viewType: Int) : this(itemView) {

            if (viewType == AppConst.TYPE_FEED_HEADER) {
                rcStories = itemView.findViewById(R.id.rc_stories)

                storyAdapter = StoryAdapter(context!!, arrStories)
                var layoutMgrStory = LinearLayoutManager(context)
                layoutMgrStory.orientation = LinearLayoutManager.HORIZONTAL
                rcStories!!.layoutManager = layoutMgrStory
                rcStories!!.adapter = storyAdapter
            }

        }


    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {


        //Header
        var imvAvatarUserFeed: ImageView? = null
        var tvFullNameFeed: TextView? = null
        var tvPubdateFeed: TextView? = null
        var imvVisibleMode: ImageView? = null
        var tvContentFeed: TextView? = null

        //Content
        var imvPhotoView1: ImageView? = null
        var imvPhotoView2: ImageView? = null
        var imvPhotoView3: ImageView? = null
        var tvMoreAlbum: TextView? = null

        //Footer
        var tvReactionCount: TextView? = null
        var tvCommentCount: TextView? = null
        var tvSharingCount: TextView? = null

        constructor(itemView: View, viewType: Int) : this(itemView) {
            //Header
            imvAvatarUserFeed = itemView.findViewById(R.id.imv_avatar_user_feed)
            tvFullNameFeed = itemView.findViewById(R.id.tv_full_name_feed)
            tvPubdateFeed = itemView.findViewById(R.id.tv_pubdate_feed)
            imvVisibleMode = itemView.findViewById(R.id.imv_visible_mode)
            tvContentFeed = itemView.findViewById(R.id.tv_content_feed)

            //Footer
            tvReactionCount = itemView.findViewById(R.id.tv_reaction_count)
            tvCommentCount = itemView.findViewById(R.id.tv_comment_count)
            tvSharingCount = itemView.findViewById(R.id.tv_sharing_count)

            //Content aa
            when (viewType) {
                AppConst.TYPE_FEED_ONE -> {
                    imvPhotoView1 = itemView.findViewById(R.id.imv_photo_view_1)
                    imvPhotoView1!!.setOnClickListener(this)
                }
                AppConst.TYPE_FEED_TWO -> {
                    imvPhotoView1 = itemView.findViewById(R.id.imv_photo_view_1)
                    imvPhotoView2 = itemView.findViewById(R.id.imv_photo_view_2)

                    imvPhotoView1!!.setOnClickListener(this)
                    imvPhotoView2!!.setOnClickListener(this)

                }
                AppConst.TYPE_FEED_THREE -> {
                    imvPhotoView1 = itemView.findViewById(R.id.imv_photo_view_1)
                    imvPhotoView2 = itemView.findViewById(R.id.imv_photo_view_2)
                    imvPhotoView3 = itemView.findViewById(R.id.imv_photo_view_3)

                    imvPhotoView1!!.setOnClickListener(this)
                    imvPhotoView2!!.setOnClickListener(this)
                    imvPhotoView3!!.setOnClickListener(this)
                }
                AppConst.TYPE_FEED_MORE -> {
                    imvPhotoView1 = itemView.findViewById(R.id.imv_photo_view_1)
                    imvPhotoView2 = itemView.findViewById(R.id.imv_photo_view_2)
                    imvPhotoView3 = itemView.findViewById(R.id.imv_photo_view_3)
                    tvMoreAlbum = itemView.findViewById(R.id.tv_more_album)

                    imvPhotoView1!!.setOnClickListener(this)
                    imvPhotoView2!!.setOnClickListener(this)
                    imvPhotoView3!!.setOnClickListener(this)
                }
            }
        }


        override fun onClick(v: View?) {
            when (v!!.id) {
                R.id.imv_photo_view_1 -> {
                    onViewPhotoFeedListenr!!.onViewPhotoFeed(0, arrNewFeeds!![adapterPosition - 1].feedImages)
                }

                R.id.imv_photo_view_2 -> {
                    onViewPhotoFeedListenr!!.onViewPhotoFeed(1, arrNewFeeds!![adapterPosition - 1].feedImages)
                }

                R.id.imv_photo_view_3 -> {
                    onViewPhotoFeedListenr!!.onViewPhotoFeed(2, arrNewFeeds!![adapterPosition - 1].feedImages)
                }
            }
        }
    }


}