package com.example.framgiamaungogiap.facebooknewfeed.app.fragment.register


import android.annotation.SuppressLint
import android.content.Intent
import android.view.View
import com.example.framgiamaungogiap.facebooknewfeed.R
import com.example.framgiamaungogiap.facebooknewfeed.app.activity.NewFeedActivity
import com.example.framgiamaungogiap.facebooknewfeed.app.base.BaseActivity
import com.example.framgiamaungogiap.facebooknewfeed.app.base.BaseFragment
import com.example.framgiamaungogiap.facebooknewfeed.model.User
import com.example.framgiamaungogiap.facebooknewfeed.utils.PreferUtils
import kotlinx.android.synthetic.main.fragment_finish_regis.*
import kotlinx.android.synthetic.main.fragment_regis_name.*
import kotlinx.android.synthetic.main.tool_bar_frag.*


@SuppressLint("ValidFragment")
class FinishRegisFragment(user: User) : BaseFragment(), View.OnClickListener {

    var user: User? = null

    init {
        this.user = user
    }

    override fun getLayoutID(): Int {
        return R.layout.fragment_finish_regis
    }

    override fun onViewReady(view: View) {
        tv_title.setText("Temp & Privacy")
        imv_back.setOnClickListener(this)
        btn_sign_up.setOnClickListener(this)
    }


    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.imv_back -> {
                backToPrevious()
            }

            R.id.btn_sign_up -> {

                PreferUtils.saveUser(context!!, user!!)

                var intent: Intent = Intent(activity, NewFeedActivity::class.java)
                startActivity(intent)
                finishActivity()
            }
        }

    }

}
