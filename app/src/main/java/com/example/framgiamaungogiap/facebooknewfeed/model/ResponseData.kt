package com.example.framgiamaungogiap.facebooknewfeed.model

import com.example.framgiamaungogiap.facebooknewfeed.utils.APIConfig
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ResponseData {

    @SerializedName(APIConfig.KEY_RESPONSE_LIST_STORIES)
    @Expose
    var storyList: ArrayList<Stories>? = null

    @SerializedName(APIConfig.KEY_RESPONSE_LIST_FEEDS)
    @Expose
    var feedList: ArrayList<Feed>? = null
}