package com.example.framgiamaungogiap.facebooknewfeed.utils

import android.content.Context
import android.content.SharedPreferences
import com.example.framgiamaungogiap.facebooknewfeed.model.User
import com.google.gson.Gson

class PreferUtils {
    companion object {

        fun saveUser(context: Context, user: User) {
            var sharePre = context.getSharedPreferences(AppConst.FILE_SETTING_NAME, Context.MODE_PRIVATE)
            var editor = sharePre.edit()
            var jsonUser = Gson().toJson(user).toString()
            editor.putString(AppConst.KEY_USER_INFOR, jsonUser)
            editor.apply()
        }

        fun getUser(context: Context): User {
            var sharePre = context.getSharedPreferences(AppConst.FILE_SETTING_NAME, Context.MODE_PRIVATE)
            var jsonUser = sharePre.getString(AppConst.KEY_USER_INFOR, "")
            if (jsonUser != "") {
                return Gson().fromJson(jsonUser, User::class.java)
            }
            return User()
        }

        fun removeUser(context: Context) {
            var sharePre = context.getSharedPreferences(AppConst.FILE_SETTING_NAME, Context.MODE_PRIVATE)
            var editor = sharePre.edit()
            editor.putString(AppConst.KEY_USER_INFOR, "")
            editor.apply()
        }

    }
}