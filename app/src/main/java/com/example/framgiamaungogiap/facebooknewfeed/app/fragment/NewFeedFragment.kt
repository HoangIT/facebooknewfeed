package com.example.framgiamaungogiap.facebooknewfeed.app.fragment


import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.example.framgiamaungogiap.facebooknewfeed.R
import com.example.framgiamaungogiap.facebooknewfeed.adapter.NewFeedAdapter
import com.example.framgiamaungogiap.facebooknewfeed.api.httpconnection.GetNewFeedAPI
import com.example.framgiamaungogiap.facebooknewfeed.api.retrofit.APIService
import com.example.framgiamaungogiap.facebooknewfeed.api.retrofit.APIUtils
import com.example.framgiamaungogiap.facebooknewfeed.app.base.BaseFragment
import com.example.framgiamaungogiap.facebooknewfeed.listener.OnRequestGetNewFeedListener
import com.example.framgiamaungogiap.facebooknewfeed.listener.OnViewPhotoFeedListenr
import com.example.framgiamaungogiap.facebooknewfeed.model.Feed
import com.example.framgiamaungogiap.facebooknewfeed.model.ResponseData
import com.example.framgiamaungogiap.facebooknewfeed.model.Stories
import com.example.framgiamaungogiap.facebooknewfeed.utils.AppConst
import com.example.framgiamaungogiap.facebooknewfeed.utils.Utilities
import kotlinx.android.synthetic.main.fragment_new_feed.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class NewFeedFragment : BaseFragment(), OnRequestGetNewFeedListener, OnViewPhotoFeedListenr {

    var arrStories = ArrayList<Stories>()
    var arrNewFeeds = ArrayList<Feed>()
    var newFeedAdapter: NewFeedAdapter? = null

    companion object {
        var fragment: NewFeedFragment? = null

        fun newInstance(): NewFeedFragment {
            var fragment = NewFeedFragment()

            return fragment
        }

        fun getInstance(): NewFeedFragment {
            return fragment!!
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        var urlApi = "http://www.mocky.io/v2/5b0ce0cb3300005100b400f1"
//        var getNewFeedAPI = GetNewFeedAPI(context!!, this)
//        getNewFeedAPI.execute(urlApi)
        showLoaddingDialog(false)
        var mAPIService = APIUtils.getAPIService().getDataNewFeed()
        var listener = object : Callback<ResponseData> {
            override fun onFailure(call: Call<ResponseData>?, t: Throwable?) {
                dismisLoaddingDialog()
                t!!.printStackTrace()
            }

            override fun onResponse(call: Call<ResponseData>?, response: Response<ResponseData>?) {
                dismisLoaddingDialog()
                if (response != null && response.body() != null) {
                    var data = response.body()
                    var listStori = data!!.storyList
                    var listNewFeed = data!!.feedList

                    arrStories.clear()
                    arrStories.addAll(listStori!!)

                    arrNewFeeds.clear()
                    arrNewFeeds.addAll(listNewFeed!!)

                    for (i in 0 until arrNewFeeds.size) {
                        var feed = arrNewFeeds.get(i)
                        when (feed.feedImages.size) {
                            0 -> feed.viewType = AppConst.TYPE_FEED_ZERO
                            1 -> feed.viewType = AppConst.TYPE_FEED_ONE
                            2 -> feed.viewType = AppConst.TYPE_FEED_TWO
                            3 -> feed.viewType = AppConst.TYPE_FEED_THREE
                            in 4..1000 -> feed.viewType = AppConst.TYPE_FEED_MORE
                        }
                    }

                    newFeedAdapter!!.notifyDataSetChanged()
                }
            }
        }
        mAPIService.enqueue(listener)
    }

    override fun getLayoutID(): Int {
        return R.layout.fragment_new_feed
    }

    override fun onViewReady(view: View) {
//        Picasso.with(context).load(File(PreferUtils.getUser(context!!).imagePath)).into(imv_avatar)

        newFeedAdapter = NewFeedAdapter(context!!, arrNewFeeds, arrStories, this)
        var layoutMgrFeed = LinearLayoutManager(context)
        layoutMgrFeed.orientation = LinearLayoutManager.VERTICAL
        rc_feed.layoutManager = layoutMgrFeed
        rc_feed.adapter = newFeedAdapter

    }


    override fun onSuccess(arrStories: ArrayList<Stories>, arrFeed: ArrayList<Feed>) {
        this.arrStories.clear()
        this.arrStories.addAll(arrStories)

        this.arrNewFeeds.clear()
        this.arrNewFeeds.addAll(arrFeed)

        newFeedAdapter!!.notifyDataSetChanged()
    }

    override fun onViewPhotoFeed(postion: Int, data: ArrayList<String>) {
        Utilities.showViewPhotoDialog(postion, activity!!, data)
    }
}
