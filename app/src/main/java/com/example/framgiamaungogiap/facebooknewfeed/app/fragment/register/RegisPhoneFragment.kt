package com.example.framgiamaungogiap.facebooknewfeed.app.fragment.register


import android.annotation.SuppressLint
import android.view.View
import com.example.framgiamaungogiap.facebooknewfeed.R
import com.example.framgiamaungogiap.facebooknewfeed.app.base.BaseFragment
import com.example.framgiamaungogiap.facebooknewfeed.model.User
import kotlinx.android.synthetic.main.fragment_regis_phone.*
import kotlinx.android.synthetic.main.tool_bar_frag.*


@SuppressLint("ValidFragment")
class RegisPhoneFragment(user: User) : BaseFragment(), View.OnClickListener {

    var user: User? = null

    init {
        this.user = user
    }

    override fun getLayoutID(): Int {
        return R.layout.fragment_regis_phone
    }

    override fun onViewReady(view: View) {
        tv_title.setText("Phone")
        imv_back.setOnClickListener(this)
        btn_next.setOnClickListener(this)
    }


    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.imv_back -> {
                backToPrevious()
            }

            R.id.btn_next -> {
                var phone = edt_phone_number.text.toString()
                if (phone == "") {
                    makeToast(context!!, "Input value")
                } else {
                    user!!.phone = phone
                    replaceFragment(R.id.fragment_container, RegisAvatarFragment(user!!), true)
                }
            }
        }

    }

}
