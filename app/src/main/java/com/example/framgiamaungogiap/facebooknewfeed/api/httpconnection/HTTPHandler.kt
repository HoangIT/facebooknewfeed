package com.example.framgiamaungogiap.facebooknewfeed.api.httpconnection

import android.util.Log
import java.io.*
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.ProtocolException
import java.net.URL

class HTTPHandler {

    val TAGG = HTTPHandler::class.java.simpleName

    fun makeServiceCall(requestUrl: String): String? {
        var response: String? = null
        try {
            var url = URL(requestUrl)
            var connection: HttpURLConnection = url.openConnection() as HttpURLConnection
            connection.requestMethod = "GET"

            var inputStream: InputStream = BufferedInputStream(connection.inputStream)
            response = converStreamToString(inputStream)
        } catch (ex: MalformedURLException) {
            Log.e(TAGG, "MalformedURLException: " + ex.message);
        } catch (ex: ProtocolException) {
            Log.e(TAGG, "ProtocolException: " + ex.message);
        } catch (ex: IOException) {
            Log.e(TAGG, "IOException: " + ex.message);
        } catch (ex: Exception) {
            Log.e(TAGG, "Exception: " + ex.message);
        }
        return response
    }


    fun converStreamToString(inputStream: InputStream): String {

        var bufferedReader: BufferedReader = BufferedReader(InputStreamReader(inputStream))
        var stringBuilder: StringBuilder = StringBuilder()

        try {
            var line: String? = null
            while (bufferedReader.readLine().let { line = it; it != null }) {
                stringBuilder.append(line)
            }

        } catch (ex: IOException) {
            ex.printStackTrace()
        } finally {
            try {
                bufferedReader.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }

        return stringBuilder.toString()
    }
}