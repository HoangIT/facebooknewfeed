package com.example.framgiamaungogiap.facebooknewfeed.app.activity

import android.support.v4.app.Fragment
import com.example.framgiamaungogiap.facebooknewfeed.R
import com.example.framgiamaungogiap.facebooknewfeed.app.base.BaseActivity
import com.example.framgiamaungogiap.facebooknewfeed.app.fragment.login.LoginFragment
import com.example.framgiamaungogiap.facebooknewfeed.extension.addFragment
import com.example.framgiamaungogiap.facebooknewfeed.extension.replaceFragment

class RegisterLoginActivity : BaseActivity() {

    override fun getLayoutID(): Int {
        return R.layout.activity_register
    }

    override fun onViewReady() {
        addFragment(R.id.fragment_container, LoginFragment(), false)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        var fragment = getVisibleFragment()
        if (fragment != null) {
            fragment.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }


    private fun getVisibleFragment(): Fragment? {
        val fragmentManager = this@RegisterLoginActivity.supportFragmentManager
        val fragments = fragmentManager.fragments
        if (fragments != null) {
            for (fragment in fragments) {
                if (fragment != null && fragment.isVisible)
                    return fragment
            }
        }
        return null
    }
}
